﻿namespace Tattoos
{
    partial class Zakupy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.textBox_kwota = new System.Windows.Forms.TextBox();
            this.textBox_opis = new System.Windows.Forms.TextBox();
            this.comboBox_dzien = new System.Windows.Forms.ComboBox();
            this.comboBox_miesiac = new System.Windows.Forms.ComboBox();
            this.comboBox_rok = new System.Windows.Forms.ComboBox();
            this.button_zapisz = new System.Windows.Forms.Button();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "Kwota";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "Opis";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(13, 17);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(38, 17);
            this.Label1.TabIndex = 35;
            this.Label1.Text = "Data";
            // 
            // textBox_kwota
            // 
            this.textBox_kwota.Location = new System.Drawing.Point(117, 106);
            this.textBox_kwota.MaxLength = 100;
            this.textBox_kwota.Name = "textBox_kwota";
            this.textBox_kwota.Size = new System.Drawing.Size(95, 22);
            this.textBox_kwota.TabIndex = 34;
            // 
            // textBox_opis
            // 
            this.textBox_opis.Location = new System.Drawing.Point(117, 59);
            this.textBox_opis.MaxLength = 100;
            this.textBox_opis.Name = "textBox_opis";
            this.textBox_opis.Size = new System.Drawing.Size(311, 22);
            this.textBox_opis.TabIndex = 33;
            // 
            // comboBox_dzien
            // 
            this.comboBox_dzien.FormattingEnabled = true;
            this.comboBox_dzien.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.comboBox_dzien.Location = new System.Drawing.Point(245, 14);
            this.comboBox_dzien.Name = "comboBox_dzien";
            this.comboBox_dzien.Size = new System.Drawing.Size(47, 24);
            this.comboBox_dzien.TabIndex = 32;
            this.comboBox_dzien.Text = "01";
            // 
            // comboBox_miesiac
            // 
            this.comboBox_miesiac.FormattingEnabled = true;
            this.comboBox_miesiac.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboBox_miesiac.Location = new System.Drawing.Point(190, 14);
            this.comboBox_miesiac.Name = "comboBox_miesiac";
            this.comboBox_miesiac.Size = new System.Drawing.Size(49, 24);
            this.comboBox_miesiac.TabIndex = 31;
            this.comboBox_miesiac.Text = "01";
            // 
            // comboBox_rok
            // 
            this.comboBox_rok.FormattingEnabled = true;
            this.comboBox_rok.Items.AddRange(new object[] {
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.comboBox_rok.Location = new System.Drawing.Point(117, 14);
            this.comboBox_rok.Name = "comboBox_rok";
            this.comboBox_rok.Size = new System.Drawing.Size(65, 24);
            this.comboBox_rok.TabIndex = 30;
            this.comboBox_rok.Text = "2018";
            // 
            // button_zapisz
            // 
            this.button_zapisz.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_zapisz.Location = new System.Drawing.Point(12, 167);
            this.button_zapisz.Name = "button_zapisz";
            this.button_zapisz.Size = new System.Drawing.Size(412, 47);
            this.button_zapisz.TabIndex = 39;
            this.button_zapisz.Text = "Zapisz";
            this.button_zapisz.UseVisualStyleBackColor = false;
            this.button_zapisz.Click += new System.EventHandler(this.button_zapisz_Click);
            // 
            // button_zamknij
            // 
            this.button_zamknij.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_zamknij.Location = new System.Drawing.Point(12, 220);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(412, 47);
            this.button_zamknij.TabIndex = 38;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = false;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // Zakupy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(436, 272);
            this.Controls.Add(this.button_zapisz);
            this.Controls.Add(this.button_zamknij);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.textBox_kwota);
            this.Controls.Add(this.textBox_opis);
            this.Controls.Add(this.comboBox_dzien);
            this.Controls.Add(this.comboBox_miesiac);
            this.Controls.Add(this.comboBox_rok);
            this.Name = "Zakupy";
            this.Text = "Zakupy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.TextBox textBox_kwota;
        private System.Windows.Forms.TextBox textBox_opis;
        private System.Windows.Forms.ComboBox comboBox_dzien;
        private System.Windows.Forms.ComboBox comboBox_miesiac;
        private System.Windows.Forms.ComboBox comboBox_rok;
        private System.Windows.Forms.Button button_zapisz;
        private System.Windows.Forms.Button button_zamknij;
    }
}