﻿namespace Tattoos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label_3RL_igla_ilosc = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_5RL_igla_ilosc = new System.Windows.Forms.Label();
            this.label_7RL_igla_ilosc = new System.Windows.Forms.Label();
            this.checkBox_3RL_igla = new System.Windows.Forms.CheckBox();
            this.numeric_3RL_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox_5RL_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_7RL_igla = new System.Windows.Forms.CheckBox();
            this.numeric_5RL_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_7RL_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.numeric_7RL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_5RL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.checkBox_7RL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_5RL_rura = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numeric_3RL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.checkBox_3RL_rura = new System.Windows.Forms.CheckBox();
            this.label_7RL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_5RL_rura_ilosc = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label_3RL_rura_ilosc = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.albel_11DT = new System.Windows.Forms.Label();
            this.label_9FL = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label_7RM_igla_ilosc = new System.Windows.Forms.Label();
            this.label_11RL_igla_ilosc = new System.Windows.Forms.Label();
            this.label_9RL_igla_ilosc = new System.Windows.Forms.Label();
            this.label_7MG_igla_ilosc = new System.Windows.Forms.Label();
            this.label_15RM_igla_ilosc = new System.Windows.Forms.Label();
            this.label_13RM_igla_ilosc = new System.Windows.Forms.Label();
            this.label_9RM_igla_ilosc = new System.Windows.Forms.Label();
            this.label_13MG_igla_ilosc = new System.Windows.Forms.Label();
            this.label_9MG_igla_ilosc = new System.Windows.Forms.Label();
            this.label_11RS_igla_ilosc = new System.Windows.Forms.Label();
            this.label_11DT_rura_ilosc = new System.Windows.Forms.Label();
            this.label_9FL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_11FL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_13FL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_7FL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_5FL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_11RL_rura_ilosc = new System.Windows.Forms.Label();
            this.label_9RL_rura_ilosc = new System.Windows.Forms.Label();
            this.checkBox_7RM_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_11RL_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_9RL_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_13MG_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_9MG_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_7MG_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_15RM_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_13RM_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_9RM_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_11RS_igla = new System.Windows.Forms.CheckBox();
            this.checkBox_11DT_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_9FL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_11FL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_13FL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_7FL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_5FL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox_11RL_rura = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.numeric_7RM_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_11RL_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_9RL_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_13MG_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_9MG_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_7MG_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_15RM_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_13RM_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_9RM_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_11RS_igla_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_11DT_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_9FL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_11FL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_13FL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_7FL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_5FL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_11RL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.numeric_9RL_rura_zuzyto = new System.Windows.Forms.NumericUpDown();
            this.label34 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_3RL_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_5RL_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7RL_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7RL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_5RL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_3RL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7RM_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11RL_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9RL_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_13MG_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9MG_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7MG_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_15RM_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_13RM_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9RM_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11RS_igla_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11DT_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9FL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11FL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_13FL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7FL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_5FL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11RL_rura_zuzyto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9RL_rura_zuzyto)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.Location = new System.Drawing.Point(11, 352);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(368, 38);
            this.button1.TabIndex = 19;
            this.button1.Text = "Zapisz";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "3RL";
            // 
            // label_3RL_igla_ilosc
            // 
            this.label_3RL_igla_ilosc.AutoSize = true;
            this.label_3RL_igla_ilosc.Location = new System.Drawing.Point(72, 32);
            this.label_3RL_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_3RL_igla_ilosc.Name = "label_3RL_igla_ilosc";
            this.label_3RL_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_3RL_igla_ilosc.TabIndex = 4;
            this.label_3RL_igla_ilosc.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "5RL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 81);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "7RL";
            // 
            // label_5RL_igla_ilosc
            // 
            this.label_5RL_igla_ilosc.AutoSize = true;
            this.label_5RL_igla_ilosc.Location = new System.Drawing.Point(72, 57);
            this.label_5RL_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_5RL_igla_ilosc.Name = "label_5RL_igla_ilosc";
            this.label_5RL_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_5RL_igla_ilosc.TabIndex = 7;
            this.label_5RL_igla_ilosc.Text = "0";
            this.label_5RL_igla_ilosc.Click += new System.EventHandler(this.label_5RL_ilosc_Click);
            // 
            // label_7RL_igla_ilosc
            // 
            this.label_7RL_igla_ilosc.AutoSize = true;
            this.label_7RL_igla_ilosc.Location = new System.Drawing.Point(72, 81);
            this.label_7RL_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_7RL_igla_ilosc.Name = "label_7RL_igla_ilosc";
            this.label_7RL_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_7RL_igla_ilosc.TabIndex = 8;
            this.label_7RL_igla_ilosc.Text = "0";
            // 
            // checkBox_3RL_igla
            // 
            this.checkBox_3RL_igla.AutoSize = true;
            this.checkBox_3RL_igla.Location = new System.Drawing.Point(107, 33);
            this.checkBox_3RL_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_3RL_igla.Name = "checkBox_3RL_igla";
            this.checkBox_3RL_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_3RL_igla.TabIndex = 11;
            this.checkBox_3RL_igla.UseVisualStyleBackColor = true;
            this.checkBox_3RL_igla.CheckedChanged += new System.EventHandler(this.checkBox_3RL_CheckedChanged);
            // 
            // numeric_3RL_igla_zuzyto
            // 
            this.numeric_3RL_igla_zuzyto.Enabled = false;
            this.numeric_3RL_igla_zuzyto.Location = new System.Drawing.Point(136, 31);
            this.numeric_3RL_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_3RL_igla_zuzyto.Name = "numeric_3RL_igla_zuzyto";
            this.numeric_3RL_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_3RL_igla_zuzyto.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Typ igły";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 7);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Ilość igieł";
            // 
            // checkBox_5RL_igla
            // 
            this.checkBox_5RL_igla.AutoSize = true;
            this.checkBox_5RL_igla.Location = new System.Drawing.Point(107, 58);
            this.checkBox_5RL_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_5RL_igla.Name = "checkBox_5RL_igla";
            this.checkBox_5RL_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_5RL_igla.TabIndex = 15;
            this.checkBox_5RL_igla.UseVisualStyleBackColor = true;
            this.checkBox_5RL_igla.CheckedChanged += new System.EventHandler(this.checkBox_5RL_CheckedChanged);
            // 
            // checkBox_7RL_igla
            // 
            this.checkBox_7RL_igla.AutoSize = true;
            this.checkBox_7RL_igla.Location = new System.Drawing.Point(107, 82);
            this.checkBox_7RL_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_7RL_igla.Name = "checkBox_7RL_igla";
            this.checkBox_7RL_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_7RL_igla.TabIndex = 16;
            this.checkBox_7RL_igla.UseVisualStyleBackColor = true;
            this.checkBox_7RL_igla.CheckedChanged += new System.EventHandler(this.checkBox_7RL_CheckedChanged);
            // 
            // numeric_5RL_igla_zuzyto
            // 
            this.numeric_5RL_igla_zuzyto.Enabled = false;
            this.numeric_5RL_igla_zuzyto.Location = new System.Drawing.Point(136, 55);
            this.numeric_5RL_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_5RL_igla_zuzyto.Name = "numeric_5RL_igla_zuzyto";
            this.numeric_5RL_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_5RL_igla_zuzyto.TabIndex = 17;
            // 
            // numeric_7RL_igla_zuzyto
            // 
            this.numeric_7RL_igla_zuzyto.Enabled = false;
            this.numeric_7RL_igla_zuzyto.Location = new System.Drawing.Point(136, 80);
            this.numeric_7RL_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_7RL_igla_zuzyto.Name = "numeric_7RL_igla_zuzyto";
            this.numeric_7RL_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_7RL_igla_zuzyto.TabIndex = 18;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button2.Location = new System.Drawing.Point(13, 395);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(367, 38);
            this.button2.TabIndex = 19;
            this.button2.Text = "Zamknij";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // numeric_7RL_rura_zuzyto
            // 
            this.numeric_7RL_rura_zuzyto.Enabled = false;
            this.numeric_7RL_rura_zuzyto.Location = new System.Drawing.Point(347, 80);
            this.numeric_7RL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_7RL_rura_zuzyto.Name = "numeric_7RL_rura_zuzyto";
            this.numeric_7RL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_7RL_rura_zuzyto.TabIndex = 34;
            // 
            // numeric_5RL_rura_zuzyto
            // 
            this.numeric_5RL_rura_zuzyto.Enabled = false;
            this.numeric_5RL_rura_zuzyto.Location = new System.Drawing.Point(347, 55);
            this.numeric_5RL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_5RL_rura_zuzyto.Name = "numeric_5RL_rura_zuzyto";
            this.numeric_5RL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_5RL_rura_zuzyto.TabIndex = 33;
            // 
            // checkBox_7RL_rura
            // 
            this.checkBox_7RL_rura.AutoSize = true;
            this.checkBox_7RL_rura.Location = new System.Drawing.Point(316, 82);
            this.checkBox_7RL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_7RL_rura.Name = "checkBox_7RL_rura";
            this.checkBox_7RL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_7RL_rura.TabIndex = 32;
            this.checkBox_7RL_rura.UseVisualStyleBackColor = true;
            this.checkBox_7RL_rura.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox_5RL_rura
            // 
            this.checkBox_5RL_rura.AutoSize = true;
            this.checkBox_5RL_rura.Location = new System.Drawing.Point(316, 58);
            this.checkBox_5RL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_5RL_rura.Name = "checkBox_5RL_rura";
            this.checkBox_5RL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_5RL_rura.TabIndex = 31;
            this.checkBox_5RL_rura.UseVisualStyleBackColor = true;
            this.checkBox_5RL_rura.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(281, 7);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Ilość rur";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(218, 7);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Typ rury";
            // 
            // numeric_3RL_rura_zuzyto
            // 
            this.numeric_3RL_rura_zuzyto.Enabled = false;
            this.numeric_3RL_rura_zuzyto.Location = new System.Drawing.Point(347, 31);
            this.numeric_3RL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_3RL_rura_zuzyto.Name = "numeric_3RL_rura_zuzyto";
            this.numeric_3RL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_3RL_rura_zuzyto.TabIndex = 28;
            // 
            // checkBox_3RL_rura
            // 
            this.checkBox_3RL_rura.AutoSize = true;
            this.checkBox_3RL_rura.Location = new System.Drawing.Point(316, 33);
            this.checkBox_3RL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_3RL_rura.Name = "checkBox_3RL_rura";
            this.checkBox_3RL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_3RL_rura.TabIndex = 27;
            this.checkBox_3RL_rura.UseVisualStyleBackColor = true;
            this.checkBox_3RL_rura.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // label_7RL_rura_ilosc
            // 
            this.label_7RL_rura_ilosc.AutoSize = true;
            this.label_7RL_rura_ilosc.Location = new System.Drawing.Point(281, 81);
            this.label_7RL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_7RL_rura_ilosc.Name = "label_7RL_rura_ilosc";
            this.label_7RL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_7RL_rura_ilosc.TabIndex = 26;
            this.label_7RL_rura_ilosc.Text = "0";
            // 
            // label_5RL_rura_ilosc
            // 
            this.label_5RL_rura_ilosc.AutoSize = true;
            this.label_5RL_rura_ilosc.Location = new System.Drawing.Point(281, 57);
            this.label_5RL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_5RL_rura_ilosc.Name = "label_5RL_rura_ilosc";
            this.label_5RL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_5RL_rura_ilosc.TabIndex = 25;
            this.label_5RL_rura_ilosc.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(218, 81);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "7RL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(218, 57);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "5RL";
            // 
            // label_3RL_rura_ilosc
            // 
            this.label_3RL_rura_ilosc.AutoSize = true;
            this.label_3RL_rura_ilosc.Location = new System.Drawing.Point(281, 32);
            this.label_3RL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_3RL_rura_ilosc.Name = "label_3RL_rura_ilosc";
            this.label_3RL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_3RL_rura_ilosc.TabIndex = 22;
            this.label_3RL_rura_ilosc.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.label13.Location = new System.Drawing.Point(218, 32);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "3RL";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 228);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "15RM 0.25";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 203);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "13RM 0.35";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 179);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "9RM 0.25";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 154);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 40;
            this.label19.Text = "7RM 0.25";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 130);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "11RL";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 106);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "9RL";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 276);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 47;
            this.label26.Text = "9MG 0.35";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 252);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 13);
            this.label27.TabIndex = 48;
            this.label27.Text = "7MG 0.25";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 301);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 13);
            this.label28.TabIndex = 49;
            this.label28.Text = "13MG 0.35";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(9, 325);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 13);
            this.label29.TabIndex = 50;
            this.label29.Text = "11RS";
            // 
            // albel_11DT
            // 
            this.albel_11DT.AutoSize = true;
            this.albel_11DT.Location = new System.Drawing.Point(218, 325);
            this.albel_11DT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.albel_11DT.Name = "albel_11DT";
            this.albel_11DT.Size = new System.Drawing.Size(34, 13);
            this.albel_11DT.TabIndex = 60;
            this.albel_11DT.Text = "11DT";
            // 
            // label_9FL
            // 
            this.label_9FL.AutoSize = true;
            this.label_9FL.Location = new System.Drawing.Point(218, 276);
            this.label_9FL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_9FL.Name = "label_9FL";
            this.label_9FL.Size = new System.Drawing.Size(25, 13);
            this.label_9FL.TabIndex = 57;
            this.label_9FL.Text = "9FL";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(218, 106);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(27, 13);
            this.label24.TabIndex = 56;
            this.label24.Text = "9RL";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(218, 130);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 13);
            this.label25.TabIndex = 55;
            this.label25.Text = "11RL";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(218, 154);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(25, 13);
            this.label30.TabIndex = 54;
            this.label30.Text = "5FL";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(218, 179);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(25, 13);
            this.label31.TabIndex = 53;
            this.label31.Text = "7FL";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(218, 203);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(31, 13);
            this.label32.TabIndex = 52;
            this.label32.Text = "13FL";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(218, 228);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(31, 13);
            this.label33.TabIndex = 51;
            this.label33.Text = "11FL";
            // 
            // label_7RM_igla_ilosc
            // 
            this.label_7RM_igla_ilosc.AutoSize = true;
            this.label_7RM_igla_ilosc.Location = new System.Drawing.Point(72, 154);
            this.label_7RM_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_7RM_igla_ilosc.Name = "label_7RM_igla_ilosc";
            this.label_7RM_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_7RM_igla_ilosc.TabIndex = 63;
            this.label_7RM_igla_ilosc.Text = "0";
            // 
            // label_11RL_igla_ilosc
            // 
            this.label_11RL_igla_ilosc.AutoSize = true;
            this.label_11RL_igla_ilosc.Location = new System.Drawing.Point(72, 130);
            this.label_11RL_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_11RL_igla_ilosc.Name = "label_11RL_igla_ilosc";
            this.label_11RL_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_11RL_igla_ilosc.TabIndex = 62;
            this.label_11RL_igla_ilosc.Text = "0";
            // 
            // label_9RL_igla_ilosc
            // 
            this.label_9RL_igla_ilosc.AutoSize = true;
            this.label_9RL_igla_ilosc.Location = new System.Drawing.Point(72, 106);
            this.label_9RL_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_9RL_igla_ilosc.Name = "label_9RL_igla_ilosc";
            this.label_9RL_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_9RL_igla_ilosc.TabIndex = 61;
            this.label_9RL_igla_ilosc.Text = "0";
            // 
            // label_7MG_igla_ilosc
            // 
            this.label_7MG_igla_ilosc.AutoSize = true;
            this.label_7MG_igla_ilosc.Location = new System.Drawing.Point(72, 252);
            this.label_7MG_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_7MG_igla_ilosc.Name = "label_7MG_igla_ilosc";
            this.label_7MG_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_7MG_igla_ilosc.TabIndex = 67;
            this.label_7MG_igla_ilosc.Text = "0";
            // 
            // label_15RM_igla_ilosc
            // 
            this.label_15RM_igla_ilosc.AutoSize = true;
            this.label_15RM_igla_ilosc.Location = new System.Drawing.Point(72, 228);
            this.label_15RM_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_15RM_igla_ilosc.Name = "label_15RM_igla_ilosc";
            this.label_15RM_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_15RM_igla_ilosc.TabIndex = 66;
            this.label_15RM_igla_ilosc.Text = "0";
            // 
            // label_13RM_igla_ilosc
            // 
            this.label_13RM_igla_ilosc.AutoSize = true;
            this.label_13RM_igla_ilosc.Location = new System.Drawing.Point(72, 203);
            this.label_13RM_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_13RM_igla_ilosc.Name = "label_13RM_igla_ilosc";
            this.label_13RM_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_13RM_igla_ilosc.TabIndex = 65;
            this.label_13RM_igla_ilosc.Text = "0";
            // 
            // label_9RM_igla_ilosc
            // 
            this.label_9RM_igla_ilosc.AutoSize = true;
            this.label_9RM_igla_ilosc.Location = new System.Drawing.Point(72, 179);
            this.label_9RM_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_9RM_igla_ilosc.Name = "label_9RM_igla_ilosc";
            this.label_9RM_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_9RM_igla_ilosc.TabIndex = 64;
            this.label_9RM_igla_ilosc.Text = "0";
            // 
            // label_13MG_igla_ilosc
            // 
            this.label_13MG_igla_ilosc.AutoSize = true;
            this.label_13MG_igla_ilosc.Location = new System.Drawing.Point(72, 301);
            this.label_13MG_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_13MG_igla_ilosc.Name = "label_13MG_igla_ilosc";
            this.label_13MG_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_13MG_igla_ilosc.TabIndex = 69;
            this.label_13MG_igla_ilosc.Text = "0";
            // 
            // label_9MG_igla_ilosc
            // 
            this.label_9MG_igla_ilosc.AutoSize = true;
            this.label_9MG_igla_ilosc.Location = new System.Drawing.Point(72, 276);
            this.label_9MG_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_9MG_igla_ilosc.Name = "label_9MG_igla_ilosc";
            this.label_9MG_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_9MG_igla_ilosc.TabIndex = 68;
            this.label_9MG_igla_ilosc.Text = "0";
            // 
            // label_11RS_igla_ilosc
            // 
            this.label_11RS_igla_ilosc.AutoSize = true;
            this.label_11RS_igla_ilosc.Location = new System.Drawing.Point(72, 325);
            this.label_11RS_igla_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_11RS_igla_ilosc.Name = "label_11RS_igla_ilosc";
            this.label_11RS_igla_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_11RS_igla_ilosc.TabIndex = 70;
            this.label_11RS_igla_ilosc.Text = "0";
            // 
            // label_11DT_rura_ilosc
            // 
            this.label_11DT_rura_ilosc.AutoSize = true;
            this.label_11DT_rura_ilosc.Location = new System.Drawing.Point(281, 325);
            this.label_11DT_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_11DT_rura_ilosc.Name = "label_11DT_rura_ilosc";
            this.label_11DT_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_11DT_rura_ilosc.TabIndex = 80;
            this.label_11DT_rura_ilosc.Text = "0";
            // 
            // label_9FL_rura_ilosc
            // 
            this.label_9FL_rura_ilosc.AutoSize = true;
            this.label_9FL_rura_ilosc.Location = new System.Drawing.Point(281, 276);
            this.label_9FL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_9FL_rura_ilosc.Name = "label_9FL_rura_ilosc";
            this.label_9FL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_9FL_rura_ilosc.TabIndex = 78;
            this.label_9FL_rura_ilosc.Text = "0";
            // 
            // label_11FL_rura_ilosc
            // 
            this.label_11FL_rura_ilosc.AutoSize = true;
            this.label_11FL_rura_ilosc.Location = new System.Drawing.Point(281, 228);
            this.label_11FL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_11FL_rura_ilosc.Name = "label_11FL_rura_ilosc";
            this.label_11FL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_11FL_rura_ilosc.TabIndex = 76;
            this.label_11FL_rura_ilosc.Text = "0";
            // 
            // label_13FL_rura_ilosc
            // 
            this.label_13FL_rura_ilosc.AutoSize = true;
            this.label_13FL_rura_ilosc.Location = new System.Drawing.Point(281, 203);
            this.label_13FL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_13FL_rura_ilosc.Name = "label_13FL_rura_ilosc";
            this.label_13FL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_13FL_rura_ilosc.TabIndex = 75;
            this.label_13FL_rura_ilosc.Text = "0";
            // 
            // label_7FL_rura_ilosc
            // 
            this.label_7FL_rura_ilosc.AutoSize = true;
            this.label_7FL_rura_ilosc.Location = new System.Drawing.Point(281, 179);
            this.label_7FL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_7FL_rura_ilosc.Name = "label_7FL_rura_ilosc";
            this.label_7FL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_7FL_rura_ilosc.TabIndex = 74;
            this.label_7FL_rura_ilosc.Text = "0";
            // 
            // label_5FL_rura_ilosc
            // 
            this.label_5FL_rura_ilosc.AutoSize = true;
            this.label_5FL_rura_ilosc.Location = new System.Drawing.Point(281, 154);
            this.label_5FL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_5FL_rura_ilosc.Name = "label_5FL_rura_ilosc";
            this.label_5FL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_5FL_rura_ilosc.TabIndex = 73;
            this.label_5FL_rura_ilosc.Text = "0";
            // 
            // label_11RL_rura_ilosc
            // 
            this.label_11RL_rura_ilosc.AutoSize = true;
            this.label_11RL_rura_ilosc.Location = new System.Drawing.Point(281, 130);
            this.label_11RL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_11RL_rura_ilosc.Name = "label_11RL_rura_ilosc";
            this.label_11RL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_11RL_rura_ilosc.TabIndex = 72;
            this.label_11RL_rura_ilosc.Text = "0";
            // 
            // label_9RL_rura_ilosc
            // 
            this.label_9RL_rura_ilosc.AutoSize = true;
            this.label_9RL_rura_ilosc.Location = new System.Drawing.Point(281, 106);
            this.label_9RL_rura_ilosc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_9RL_rura_ilosc.Name = "label_9RL_rura_ilosc";
            this.label_9RL_rura_ilosc.Size = new System.Drawing.Size(13, 13);
            this.label_9RL_rura_ilosc.TabIndex = 71;
            this.label_9RL_rura_ilosc.Text = "0";
            // 
            // checkBox_7RM_igla
            // 
            this.checkBox_7RM_igla.AutoSize = true;
            this.checkBox_7RM_igla.Location = new System.Drawing.Point(107, 155);
            this.checkBox_7RM_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_7RM_igla.Name = "checkBox_7RM_igla";
            this.checkBox_7RM_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_7RM_igla.TabIndex = 83;
            this.checkBox_7RM_igla.UseVisualStyleBackColor = true;
            this.checkBox_7RM_igla.CheckedChanged += new System.EventHandler(this.checkBox_7RM_igla_CheckedChanged);
            // 
            // checkBox_11RL_igla
            // 
            this.checkBox_11RL_igla.AutoSize = true;
            this.checkBox_11RL_igla.Location = new System.Drawing.Point(107, 131);
            this.checkBox_11RL_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_11RL_igla.Name = "checkBox_11RL_igla";
            this.checkBox_11RL_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_11RL_igla.TabIndex = 82;
            this.checkBox_11RL_igla.UseVisualStyleBackColor = true;
            this.checkBox_11RL_igla.CheckedChanged += new System.EventHandler(this.checkBox_11RL_igla_CheckedChanged);
            // 
            // checkBox_9RL_igla
            // 
            this.checkBox_9RL_igla.AutoSize = true;
            this.checkBox_9RL_igla.Location = new System.Drawing.Point(107, 106);
            this.checkBox_9RL_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_9RL_igla.Name = "checkBox_9RL_igla";
            this.checkBox_9RL_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_9RL_igla.TabIndex = 81;
            this.checkBox_9RL_igla.UseVisualStyleBackColor = true;
            this.checkBox_9RL_igla.CheckedChanged += new System.EventHandler(this.checkBox_9RL_igla_CheckedChanged);
            // 
            // checkBox_13MG_igla
            // 
            this.checkBox_13MG_igla.AutoSize = true;
            this.checkBox_13MG_igla.Location = new System.Drawing.Point(107, 301);
            this.checkBox_13MG_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_13MG_igla.Name = "checkBox_13MG_igla";
            this.checkBox_13MG_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_13MG_igla.TabIndex = 89;
            this.checkBox_13MG_igla.UseVisualStyleBackColor = true;
            this.checkBox_13MG_igla.CheckedChanged += new System.EventHandler(this.checkBox_13MG_igla_CheckedChanged);
            // 
            // checkBox_9MG_igla
            // 
            this.checkBox_9MG_igla.AutoSize = true;
            this.checkBox_9MG_igla.Location = new System.Drawing.Point(107, 277);
            this.checkBox_9MG_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_9MG_igla.Name = "checkBox_9MG_igla";
            this.checkBox_9MG_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_9MG_igla.TabIndex = 88;
            this.checkBox_9MG_igla.UseVisualStyleBackColor = true;
            this.checkBox_9MG_igla.CheckedChanged += new System.EventHandler(this.checkBox_9MG_igla_CheckedChanged);
            // 
            // checkBox_7MG_igla
            // 
            this.checkBox_7MG_igla.AutoSize = true;
            this.checkBox_7MG_igla.Location = new System.Drawing.Point(107, 253);
            this.checkBox_7MG_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_7MG_igla.Name = "checkBox_7MG_igla";
            this.checkBox_7MG_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_7MG_igla.TabIndex = 87;
            this.checkBox_7MG_igla.UseVisualStyleBackColor = true;
            this.checkBox_7MG_igla.CheckedChanged += new System.EventHandler(this.checkBox_7MG_igla_CheckedChanged);
            // 
            // checkBox_15RM_igla
            // 
            this.checkBox_15RM_igla.AutoSize = true;
            this.checkBox_15RM_igla.Location = new System.Drawing.Point(107, 228);
            this.checkBox_15RM_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_15RM_igla.Name = "checkBox_15RM_igla";
            this.checkBox_15RM_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_15RM_igla.TabIndex = 86;
            this.checkBox_15RM_igla.UseVisualStyleBackColor = true;
            this.checkBox_15RM_igla.CheckedChanged += new System.EventHandler(this.checkBox_15RM_igla_CheckedChanged);
            // 
            // checkBox_13RM_igla
            // 
            this.checkBox_13RM_igla.AutoSize = true;
            this.checkBox_13RM_igla.Location = new System.Drawing.Point(107, 204);
            this.checkBox_13RM_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_13RM_igla.Name = "checkBox_13RM_igla";
            this.checkBox_13RM_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_13RM_igla.TabIndex = 85;
            this.checkBox_13RM_igla.UseVisualStyleBackColor = true;
            this.checkBox_13RM_igla.CheckedChanged += new System.EventHandler(this.checkBox_13RM_igla_CheckedChanged);
            // 
            // checkBox_9RM_igla
            // 
            this.checkBox_9RM_igla.AutoSize = true;
            this.checkBox_9RM_igla.Location = new System.Drawing.Point(107, 180);
            this.checkBox_9RM_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_9RM_igla.Name = "checkBox_9RM_igla";
            this.checkBox_9RM_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_9RM_igla.TabIndex = 84;
            this.checkBox_9RM_igla.UseVisualStyleBackColor = true;
            this.checkBox_9RM_igla.CheckedChanged += new System.EventHandler(this.checkBox_9RM_igla_CheckedChanged);
            // 
            // checkBox_11RS_igla
            // 
            this.checkBox_11RS_igla.AutoSize = true;
            this.checkBox_11RS_igla.Location = new System.Drawing.Point(107, 326);
            this.checkBox_11RS_igla.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_11RS_igla.Name = "checkBox_11RS_igla";
            this.checkBox_11RS_igla.Size = new System.Drawing.Size(15, 14);
            this.checkBox_11RS_igla.TabIndex = 90;
            this.checkBox_11RS_igla.UseVisualStyleBackColor = true;
            this.checkBox_11RS_igla.CheckedChanged += new System.EventHandler(this.checkBox_11RS_igla_CheckedChanged);
            // 
            // checkBox_11DT_rura
            // 
            this.checkBox_11DT_rura.AutoSize = true;
            this.checkBox_11DT_rura.Location = new System.Drawing.Point(316, 326);
            this.checkBox_11DT_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_11DT_rura.Name = "checkBox_11DT_rura";
            this.checkBox_11DT_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_11DT_rura.TabIndex = 100;
            this.checkBox_11DT_rura.UseVisualStyleBackColor = true;
            this.checkBox_11DT_rura.CheckedChanged += new System.EventHandler(this.checkBox_11DT_rura_CheckedChanged);
            // 
            // checkBox_9FL_rura
            // 
            this.checkBox_9FL_rura.AutoSize = true;
            this.checkBox_9FL_rura.Location = new System.Drawing.Point(316, 277);
            this.checkBox_9FL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_9FL_rura.Name = "checkBox_9FL_rura";
            this.checkBox_9FL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_9FL_rura.TabIndex = 98;
            this.checkBox_9FL_rura.UseVisualStyleBackColor = true;
            this.checkBox_9FL_rura.CheckedChanged += new System.EventHandler(this.checkBox_9FL_rura_CheckedChanged);
            // 
            // checkBox_11FL_rura
            // 
            this.checkBox_11FL_rura.AutoSize = true;
            this.checkBox_11FL_rura.Location = new System.Drawing.Point(316, 228);
            this.checkBox_11FL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_11FL_rura.Name = "checkBox_11FL_rura";
            this.checkBox_11FL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_11FL_rura.TabIndex = 96;
            this.checkBox_11FL_rura.UseVisualStyleBackColor = true;
            this.checkBox_11FL_rura.CheckedChanged += new System.EventHandler(this.checkBox_11FL_rura_CheckedChanged);
            // 
            // checkBox_13FL_rura
            // 
            this.checkBox_13FL_rura.AutoSize = true;
            this.checkBox_13FL_rura.Location = new System.Drawing.Point(316, 204);
            this.checkBox_13FL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_13FL_rura.Name = "checkBox_13FL_rura";
            this.checkBox_13FL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_13FL_rura.TabIndex = 95;
            this.checkBox_13FL_rura.UseVisualStyleBackColor = true;
            this.checkBox_13FL_rura.CheckedChanged += new System.EventHandler(this.checkBox_13FL_rura_CheckedChanged);
            // 
            // checkBox_7FL_rura
            // 
            this.checkBox_7FL_rura.AutoSize = true;
            this.checkBox_7FL_rura.Location = new System.Drawing.Point(316, 180);
            this.checkBox_7FL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_7FL_rura.Name = "checkBox_7FL_rura";
            this.checkBox_7FL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_7FL_rura.TabIndex = 94;
            this.checkBox_7FL_rura.UseVisualStyleBackColor = true;
            this.checkBox_7FL_rura.CheckedChanged += new System.EventHandler(this.checkBox_7FL_rura_CheckedChanged);
            // 
            // checkBox_5FL_rura
            // 
            this.checkBox_5FL_rura.AutoSize = true;
            this.checkBox_5FL_rura.Location = new System.Drawing.Point(316, 155);
            this.checkBox_5FL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_5FL_rura.Name = "checkBox_5FL_rura";
            this.checkBox_5FL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_5FL_rura.TabIndex = 93;
            this.checkBox_5FL_rura.UseVisualStyleBackColor = true;
            this.checkBox_5FL_rura.CheckedChanged += new System.EventHandler(this.checkBox_5FL_rura_CheckedChanged);
            // 
            // checkBox_11RL_rura
            // 
            this.checkBox_11RL_rura.AutoSize = true;
            this.checkBox_11RL_rura.Location = new System.Drawing.Point(316, 131);
            this.checkBox_11RL_rura.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_11RL_rura.Name = "checkBox_11RL_rura";
            this.checkBox_11RL_rura.Size = new System.Drawing.Size(15, 14);
            this.checkBox_11RL_rura.TabIndex = 92;
            this.checkBox_11RL_rura.UseVisualStyleBackColor = true;
            this.checkBox_11RL_rura.CheckedChanged += new System.EventHandler(this.checkBox_11RL_rura_CheckedChanged);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(316, 106);
            this.checkBox20.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(15, 14);
            this.checkBox20.TabIndex = 91;
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.checkBox20_CheckedChanged);
            // 
            // numeric_7RM_igla_zuzyto
            // 
            this.numeric_7RM_igla_zuzyto.Enabled = false;
            this.numeric_7RM_igla_zuzyto.Location = new System.Drawing.Point(136, 153);
            this.numeric_7RM_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_7RM_igla_zuzyto.Name = "numeric_7RM_igla_zuzyto";
            this.numeric_7RM_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_7RM_igla_zuzyto.TabIndex = 103;
            // 
            // numeric_11RL_igla_zuzyto
            // 
            this.numeric_11RL_igla_zuzyto.Enabled = false;
            this.numeric_11RL_igla_zuzyto.Location = new System.Drawing.Point(136, 128);
            this.numeric_11RL_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_11RL_igla_zuzyto.Name = "numeric_11RL_igla_zuzyto";
            this.numeric_11RL_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_11RL_igla_zuzyto.TabIndex = 102;
            // 
            // numeric_9RL_igla_zuzyto
            // 
            this.numeric_9RL_igla_zuzyto.Enabled = false;
            this.numeric_9RL_igla_zuzyto.Location = new System.Drawing.Point(136, 104);
            this.numeric_9RL_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_9RL_igla_zuzyto.Name = "numeric_9RL_igla_zuzyto";
            this.numeric_9RL_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_9RL_igla_zuzyto.TabIndex = 101;
            // 
            // numeric_13MG_igla_zuzyto
            // 
            this.numeric_13MG_igla_zuzyto.Enabled = false;
            this.numeric_13MG_igla_zuzyto.Location = new System.Drawing.Point(136, 299);
            this.numeric_13MG_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_13MG_igla_zuzyto.Name = "numeric_13MG_igla_zuzyto";
            this.numeric_13MG_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_13MG_igla_zuzyto.TabIndex = 109;
            // 
            // numeric_9MG_igla_zuzyto
            // 
            this.numeric_9MG_igla_zuzyto.Enabled = false;
            this.numeric_9MG_igla_zuzyto.Location = new System.Drawing.Point(136, 275);
            this.numeric_9MG_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_9MG_igla_zuzyto.Name = "numeric_9MG_igla_zuzyto";
            this.numeric_9MG_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_9MG_igla_zuzyto.TabIndex = 108;
            // 
            // numeric_7MG_igla_zuzyto
            // 
            this.numeric_7MG_igla_zuzyto.Enabled = false;
            this.numeric_7MG_igla_zuzyto.Location = new System.Drawing.Point(136, 250);
            this.numeric_7MG_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_7MG_igla_zuzyto.Name = "numeric_7MG_igla_zuzyto";
            this.numeric_7MG_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_7MG_igla_zuzyto.TabIndex = 107;
            // 
            // numeric_15RM_igla_zuzyto
            // 
            this.numeric_15RM_igla_zuzyto.Enabled = false;
            this.numeric_15RM_igla_zuzyto.Location = new System.Drawing.Point(136, 226);
            this.numeric_15RM_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_15RM_igla_zuzyto.Name = "numeric_15RM_igla_zuzyto";
            this.numeric_15RM_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_15RM_igla_zuzyto.TabIndex = 106;
            // 
            // numeric_13RM_igla_zuzyto
            // 
            this.numeric_13RM_igla_zuzyto.Enabled = false;
            this.numeric_13RM_igla_zuzyto.Location = new System.Drawing.Point(136, 202);
            this.numeric_13RM_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_13RM_igla_zuzyto.Name = "numeric_13RM_igla_zuzyto";
            this.numeric_13RM_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_13RM_igla_zuzyto.TabIndex = 105;
            // 
            // numeric_9RM_igla_zuzyto
            // 
            this.numeric_9RM_igla_zuzyto.Enabled = false;
            this.numeric_9RM_igla_zuzyto.Location = new System.Drawing.Point(136, 177);
            this.numeric_9RM_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_9RM_igla_zuzyto.Name = "numeric_9RM_igla_zuzyto";
            this.numeric_9RM_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_9RM_igla_zuzyto.TabIndex = 104;
            // 
            // numeric_11RS_igla_zuzyto
            // 
            this.numeric_11RS_igla_zuzyto.Enabled = false;
            this.numeric_11RS_igla_zuzyto.Location = new System.Drawing.Point(136, 323);
            this.numeric_11RS_igla_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_11RS_igla_zuzyto.Name = "numeric_11RS_igla_zuzyto";
            this.numeric_11RS_igla_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_11RS_igla_zuzyto.TabIndex = 110;
            // 
            // numeric_11DT_rura_zuzyto
            // 
            this.numeric_11DT_rura_zuzyto.Enabled = false;
            this.numeric_11DT_rura_zuzyto.Location = new System.Drawing.Point(347, 323);
            this.numeric_11DT_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_11DT_rura_zuzyto.Name = "numeric_11DT_rura_zuzyto";
            this.numeric_11DT_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_11DT_rura_zuzyto.TabIndex = 120;
            // 
            // numeric_9FL_rura_zuzyto
            // 
            this.numeric_9FL_rura_zuzyto.Enabled = false;
            this.numeric_9FL_rura_zuzyto.Location = new System.Drawing.Point(347, 275);
            this.numeric_9FL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_9FL_rura_zuzyto.Name = "numeric_9FL_rura_zuzyto";
            this.numeric_9FL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_9FL_rura_zuzyto.TabIndex = 118;
            // 
            // numeric_11FL_rura_zuzyto
            // 
            this.numeric_11FL_rura_zuzyto.Enabled = false;
            this.numeric_11FL_rura_zuzyto.Location = new System.Drawing.Point(347, 226);
            this.numeric_11FL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_11FL_rura_zuzyto.Name = "numeric_11FL_rura_zuzyto";
            this.numeric_11FL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_11FL_rura_zuzyto.TabIndex = 116;
            // 
            // numeric_13FL_rura_zuzyto
            // 
            this.numeric_13FL_rura_zuzyto.Enabled = false;
            this.numeric_13FL_rura_zuzyto.Location = new System.Drawing.Point(347, 202);
            this.numeric_13FL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_13FL_rura_zuzyto.Name = "numeric_13FL_rura_zuzyto";
            this.numeric_13FL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_13FL_rura_zuzyto.TabIndex = 115;
            // 
            // numeric_7FL_rura_zuzyto
            // 
            this.numeric_7FL_rura_zuzyto.Enabled = false;
            this.numeric_7FL_rura_zuzyto.Location = new System.Drawing.Point(347, 177);
            this.numeric_7FL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_7FL_rura_zuzyto.Name = "numeric_7FL_rura_zuzyto";
            this.numeric_7FL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_7FL_rura_zuzyto.TabIndex = 114;
            // 
            // numeric_5FL_rura_zuzyto
            // 
            this.numeric_5FL_rura_zuzyto.Enabled = false;
            this.numeric_5FL_rura_zuzyto.Location = new System.Drawing.Point(347, 153);
            this.numeric_5FL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_5FL_rura_zuzyto.Name = "numeric_5FL_rura_zuzyto";
            this.numeric_5FL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_5FL_rura_zuzyto.TabIndex = 113;
            // 
            // numeric_11RL_rura_zuzyto
            // 
            this.numeric_11RL_rura_zuzyto.Enabled = false;
            this.numeric_11RL_rura_zuzyto.Location = new System.Drawing.Point(347, 128);
            this.numeric_11RL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_11RL_rura_zuzyto.Name = "numeric_11RL_rura_zuzyto";
            this.numeric_11RL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_11RL_rura_zuzyto.TabIndex = 112;
            // 
            // numeric_9RL_rura_zuzyto
            // 
            this.numeric_9RL_rura_zuzyto.Enabled = false;
            this.numeric_9RL_rura_zuzyto.Location = new System.Drawing.Point(347, 104);
            this.numeric_9RL_rura_zuzyto.Margin = new System.Windows.Forms.Padding(2);
            this.numeric_9RL_rura_zuzyto.Name = "numeric_9RL_rura_zuzyto";
            this.numeric_9RL_rura_zuzyto.Size = new System.Drawing.Size(32, 20);
            this.numeric_9RL_rura_zuzyto.TabIndex = 111;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(10, 375);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(0, 13);
            this.label34.TabIndex = 121;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(683, 588);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.numeric_11DT_rura_zuzyto);
            this.Controls.Add(this.numeric_9FL_rura_zuzyto);
            this.Controls.Add(this.numeric_11FL_rura_zuzyto);
            this.Controls.Add(this.numeric_13FL_rura_zuzyto);
            this.Controls.Add(this.numeric_7FL_rura_zuzyto);
            this.Controls.Add(this.numeric_5FL_rura_zuzyto);
            this.Controls.Add(this.numeric_11RL_rura_zuzyto);
            this.Controls.Add(this.numeric_9RL_rura_zuzyto);
            this.Controls.Add(this.numeric_11RS_igla_zuzyto);
            this.Controls.Add(this.numeric_13MG_igla_zuzyto);
            this.Controls.Add(this.numeric_9MG_igla_zuzyto);
            this.Controls.Add(this.numeric_7MG_igla_zuzyto);
            this.Controls.Add(this.numeric_15RM_igla_zuzyto);
            this.Controls.Add(this.numeric_13RM_igla_zuzyto);
            this.Controls.Add(this.numeric_9RM_igla_zuzyto);
            this.Controls.Add(this.numeric_7RM_igla_zuzyto);
            this.Controls.Add(this.numeric_11RL_igla_zuzyto);
            this.Controls.Add(this.numeric_9RL_igla_zuzyto);
            this.Controls.Add(this.checkBox_11DT_rura);
            this.Controls.Add(this.checkBox_9FL_rura);
            this.Controls.Add(this.checkBox_11FL_rura);
            this.Controls.Add(this.checkBox_13FL_rura);
            this.Controls.Add(this.checkBox_7FL_rura);
            this.Controls.Add(this.checkBox_5FL_rura);
            this.Controls.Add(this.checkBox_11RL_rura);
            this.Controls.Add(this.checkBox20);
            this.Controls.Add(this.checkBox_11RS_igla);
            this.Controls.Add(this.checkBox_13MG_igla);
            this.Controls.Add(this.checkBox_9MG_igla);
            this.Controls.Add(this.checkBox_7MG_igla);
            this.Controls.Add(this.checkBox_15RM_igla);
            this.Controls.Add(this.checkBox_13RM_igla);
            this.Controls.Add(this.checkBox_9RM_igla);
            this.Controls.Add(this.checkBox_7RM_igla);
            this.Controls.Add(this.checkBox_11RL_igla);
            this.Controls.Add(this.checkBox_9RL_igla);
            this.Controls.Add(this.label_11DT_rura_ilosc);
            this.Controls.Add(this.label_9FL_rura_ilosc);
            this.Controls.Add(this.label_11FL_rura_ilosc);
            this.Controls.Add(this.label_13FL_rura_ilosc);
            this.Controls.Add(this.label_7FL_rura_ilosc);
            this.Controls.Add(this.label_5FL_rura_ilosc);
            this.Controls.Add(this.label_11RL_rura_ilosc);
            this.Controls.Add(this.label_9RL_rura_ilosc);
            this.Controls.Add(this.label_11RS_igla_ilosc);
            this.Controls.Add(this.label_13MG_igla_ilosc);
            this.Controls.Add(this.label_9MG_igla_ilosc);
            this.Controls.Add(this.label_7MG_igla_ilosc);
            this.Controls.Add(this.label_15RM_igla_ilosc);
            this.Controls.Add(this.label_13RM_igla_ilosc);
            this.Controls.Add(this.label_9RM_igla_ilosc);
            this.Controls.Add(this.label_7RM_igla_ilosc);
            this.Controls.Add(this.label_11RL_igla_ilosc);
            this.Controls.Add(this.label_9RL_igla_ilosc);
            this.Controls.Add(this.albel_11DT);
            this.Controls.Add(this.label_9FL);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.numeric_7RL_rura_zuzyto);
            this.Controls.Add(this.numeric_5RL_rura_zuzyto);
            this.Controls.Add(this.checkBox_7RL_rura);
            this.Controls.Add(this.checkBox_5RL_rura);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numeric_3RL_rura_zuzyto);
            this.Controls.Add(this.checkBox_3RL_rura);
            this.Controls.Add(this.label_7RL_rura_ilosc);
            this.Controls.Add(this.label_5RL_rura_ilosc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label_3RL_rura_ilosc);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.numeric_7RL_igla_zuzyto);
            this.Controls.Add(this.numeric_5RL_igla_zuzyto);
            this.Controls.Add(this.checkBox_7RL_igla);
            this.Controls.Add(this.checkBox_5RL_igla);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numeric_3RL_igla_zuzyto);
            this.Controls.Add(this.checkBox_3RL_igla);
            this.Controls.Add(this.label_7RL_igla_ilosc);
            this.Controls.Add(this.label_5RL_igla_ilosc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_3RL_igla_ilosc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Asortyment";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numeric_3RL_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_5RL_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7RL_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7RL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_5RL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_3RL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7RM_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11RL_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9RL_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_13MG_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9MG_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7MG_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_15RM_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_13RM_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9RM_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11RS_igla_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11DT_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9FL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11FL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_13FL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_7FL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_5FL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_11RL_rura_zuzyto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_9RL_rura_zuzyto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_5RL_igla_ilosc;
        private System.Windows.Forms.Label label_7RL_igla_ilosc;
        public System.Windows.Forms.Label label_3RL_igla_ilosc;
        private System.Windows.Forms.CheckBox checkBox_3RL_igla;
        private System.Windows.Forms.NumericUpDown numeric_3RL_igla_zuzyto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox_5RL_igla;
        private System.Windows.Forms.CheckBox checkBox_7RL_igla;
        private System.Windows.Forms.NumericUpDown numeric_5RL_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_7RL_igla_zuzyto;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown numeric_7RL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_5RL_rura_zuzyto;
        private System.Windows.Forms.CheckBox checkBox_7RL_rura;
        private System.Windows.Forms.CheckBox checkBox_5RL_rura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numeric_3RL_rura_zuzyto;
        private System.Windows.Forms.CheckBox checkBox_3RL_rura;
        private System.Windows.Forms.Label label_7RL_rura_ilosc;
        private System.Windows.Forms.Label label_5RL_rura_ilosc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label_3RL_rura_ilosc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label albel_11DT;
        private System.Windows.Forms.Label label_9FL;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label_7RM_igla_ilosc;
        private System.Windows.Forms.Label label_11RL_igla_ilosc;
        public System.Windows.Forms.Label label_9RL_igla_ilosc;
        private System.Windows.Forms.Label label_7MG_igla_ilosc;
        private System.Windows.Forms.Label label_15RM_igla_ilosc;
        public System.Windows.Forms.Label label_13RM_igla_ilosc;
        private System.Windows.Forms.Label label_9RM_igla_ilosc;
        private System.Windows.Forms.Label label_13MG_igla_ilosc;
        private System.Windows.Forms.Label label_9MG_igla_ilosc;
        private System.Windows.Forms.Label label_11RS_igla_ilosc;
        private System.Windows.Forms.Label label_11DT_rura_ilosc;
        private System.Windows.Forms.Label label_9FL_rura_ilosc;
        private System.Windows.Forms.Label label_11FL_rura_ilosc;
        public System.Windows.Forms.Label label_13FL_rura_ilosc;
        private System.Windows.Forms.Label label_7FL_rura_ilosc;
        private System.Windows.Forms.Label label_5FL_rura_ilosc;
        private System.Windows.Forms.Label label_11RL_rura_ilosc;
        public System.Windows.Forms.Label label_9RL_rura_ilosc;
        private System.Windows.Forms.CheckBox checkBox_7RM_igla;
        private System.Windows.Forms.CheckBox checkBox_11RL_igla;
        private System.Windows.Forms.CheckBox checkBox_9RL_igla;
        private System.Windows.Forms.CheckBox checkBox_13MG_igla;
        private System.Windows.Forms.CheckBox checkBox_9MG_igla;
        private System.Windows.Forms.CheckBox checkBox_7MG_igla;
        private System.Windows.Forms.CheckBox checkBox_15RM_igla;
        private System.Windows.Forms.CheckBox checkBox_13RM_igla;
        private System.Windows.Forms.CheckBox checkBox_9RM_igla;
        private System.Windows.Forms.CheckBox checkBox_11RS_igla;
        private System.Windows.Forms.CheckBox checkBox_11DT_rura;
        private System.Windows.Forms.CheckBox checkBox_9FL_rura;
        private System.Windows.Forms.CheckBox checkBox_11FL_rura;
        private System.Windows.Forms.CheckBox checkBox_13FL_rura;
        private System.Windows.Forms.CheckBox checkBox_7FL_rura;
        private System.Windows.Forms.CheckBox checkBox_5FL_rura;
        private System.Windows.Forms.CheckBox checkBox_11RL_rura;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.NumericUpDown numeric_7RM_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_11RL_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_9RL_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_13MG_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_9MG_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_7MG_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_15RM_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_13RM_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_9RM_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_11RS_igla_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_11DT_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_9FL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_11FL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_13FL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_7FL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_5FL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_11RL_rura_zuzyto;
        private System.Windows.Forms.NumericUpDown numeric_9RL_rura_zuzyto;
        private System.Windows.Forms.Label label34;
    }
}

