﻿namespace Tattoos
{
    partial class Dane
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_miesiac = new System.Windows.Forms.ComboBox();
            this.comboBox_rok = new System.Windows.Forms.ComboBox();
            this.label_przychod = new System.Windows.Forms.Label();
            this.label_koszty = new System.Windows.Forms.Label();
            this.label_dochod = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label_ilosc_tatuazy = new System.Windows.Forms.Label();
            this.label_srednia_cena_za_tatuaz = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Przychód";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Koszty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 110);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Dochód";
            // 
            // comboBox_miesiac
            // 
            this.comboBox_miesiac.FormattingEnabled = true;
            this.comboBox_miesiac.Items.AddRange(new object[] {
            "Styczeń",
            "Luty",
            "Marzec",
            "Kwiecień",
            "Maj",
            "Czerwiec",
            "Lipiec",
            "Sierpień",
            "Wrzesień",
            "Październik",
            "Listopad",
            "Grudzień"});
            this.comboBox_miesiac.Location = new System.Drawing.Point(151, 10);
            this.comboBox_miesiac.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox_miesiac.Name = "comboBox_miesiac";
            this.comboBox_miesiac.Size = new System.Drawing.Size(80, 21);
            this.comboBox_miesiac.TabIndex = 3;
            this.comboBox_miesiac.Text = "Styczeń";
            this.comboBox_miesiac.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox_rok
            // 
            this.comboBox_rok.FormattingEnabled = true;
            this.comboBox_rok.Items.AddRange(new object[] {
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.comboBox_rok.Location = new System.Drawing.Point(98, 10);
            this.comboBox_rok.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox_rok.Name = "comboBox_rok";
            this.comboBox_rok.Size = new System.Drawing.Size(49, 21);
            this.comboBox_rok.TabIndex = 4;
            this.comboBox_rok.Text = "2018";
            // 
            // label_przychod
            // 
            this.label_przychod.AutoSize = true;
            this.label_przychod.Location = new System.Drawing.Point(170, 50);
            this.label_przychod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_przychod.Name = "label_przychod";
            this.label_przychod.Size = new System.Drawing.Size(13, 13);
            this.label_przychod.TabIndex = 5;
            this.label_przychod.Text = "0";
            // 
            // label_koszty
            // 
            this.label_koszty.AutoSize = true;
            this.label_koszty.Location = new System.Drawing.Point(170, 80);
            this.label_koszty.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_koszty.Name = "label_koszty";
            this.label_koszty.Size = new System.Drawing.Size(13, 13);
            this.label_koszty.TabIndex = 6;
            this.label_koszty.Text = "0";
            // 
            // label_dochod
            // 
            this.label_dochod.AutoSize = true;
            this.label_dochod.Location = new System.Drawing.Point(170, 110);
            this.label_dochod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_dochod.Name = "label_dochod";
            this.label_dochod.Size = new System.Drawing.Size(13, 13);
            this.label_dochod.TabIndex = 7;
            this.label_dochod.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 140);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ilość tatuaży";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 170);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Średnia cena za tatuaż";
            // 
            // label_ilosc_tatuazy
            // 
            this.label_ilosc_tatuazy.AutoSize = true;
            this.label_ilosc_tatuazy.Location = new System.Drawing.Point(170, 140);
            this.label_ilosc_tatuazy.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_ilosc_tatuazy.Name = "label_ilosc_tatuazy";
            this.label_ilosc_tatuazy.Size = new System.Drawing.Size(13, 13);
            this.label_ilosc_tatuazy.TabIndex = 32;
            this.label_ilosc_tatuazy.Text = "0";
            // 
            // label_srednia_cena_za_tatuaz
            // 
            this.label_srednia_cena_za_tatuaz.AutoSize = true;
            this.label_srednia_cena_za_tatuaz.Location = new System.Drawing.Point(170, 170);
            this.label_srednia_cena_za_tatuaz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_srednia_cena_za_tatuaz.Name = "label_srednia_cena_za_tatuaz";
            this.label_srednia_cena_za_tatuaz.Size = new System.Drawing.Size(13, 13);
            this.label_srednia_cena_za_tatuaz.TabIndex = 35;
            this.label_srednia_cena_za_tatuaz.Text = "0";
            // 
            // Dane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(608, 384);
            this.Controls.Add(this.label_srednia_cena_za_tatuaz);
            this.Controls.Add(this.label_ilosc_tatuazy);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_dochod);
            this.Controls.Add(this.label_koszty);
            this.Controls.Add(this.label_przychod);
            this.Controls.Add(this.comboBox_rok);
            this.Controls.Add(this.comboBox_miesiac);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Dane";
            this.Text = "Dane";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_miesiac;
        private System.Windows.Forms.ComboBox comboBox_rok;
        private System.Windows.Forms.Label label_przychod;
        private System.Windows.Forms.Label label_koszty;
        private System.Windows.Forms.Label label_dochod;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_ilosc_tatuazy;
        private System.Windows.Forms.Label label_srednia_cena_za_tatuaz;
    }
}