﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Tattoos
{
    public partial class Zakupy : Form
    {
        

        int id;
        String miesiac;
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-FDSHGHC;Initial Catalog=Tattoos_Database;Integrated Security=True");

        public Zakupy()
        {
            InitializeComponent();
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            this.Hide();
            Start st = new Start();
            st.Show();
        }

        private void button_zapisz_Click(object sender, EventArgs e)
        {
            if ((textBox_opis.Text == "") || (textBox_kwota.Text == ""))
            {
                MessageBox.Show("Wypełnij wszystkie pola");
            }
            else
            {
                con.Open();
                String qry = "Select max(Id) from Table_Koszty";
                SqlCommand command = new SqlCommand(qry, con);
                id = (int)command.ExecuteScalar() + 1;
                jaki_miesiac(comboBox_miesiac);

                qry = "Insert into Table_Koszty (Id,Data, Miesiac,Opis,Kwota) values (" + id + ",'" + comboBox_rok.Text + "-" + comboBox_miesiac.Text + "-" + comboBox_dzien.Text + "','" + miesiac + "','" + textBox_opis.Text + "'," + textBox_kwota.Text + ")";
                command = new SqlCommand(qry, con);
                command.ExecuteNonQuery();
                con.Close();

                textBox_opis.Text = "";
                textBox_kwota.Text = "";
                MessageBox.Show("Zapisano");
            }
        }

        void jaki_miesiac(ComboBox data)
        {
            if (data.Text == "01")
            {
                miesiac = "Styczeń";
            }
            if (data.Text == "02")
            {
                miesiac = "Luty";
            }
            if (data.Text == "03")
            {
                miesiac = "Marzec";
            }
            if (data.Text == "04")
            {
                miesiac = "Kwiecień";
            }
            if (data.Text == "05")
            {
                miesiac = "Maj";
            }
            if (data.Text == "06")
            {
                miesiac = "Czerwiec";
            }
            if (data.Text == "07")
            {
                miesiac = "Lipiec";
            }
            if (data.Text == "08")
            {
                miesiac = "Sierpień";
            }
            if (data.Text == "09")
            {
                miesiac = "Wrzesień";
            }
            if (data.Text == "10")
            {
                miesiac = "Październik";
            }
            if (data.Text == "11")
            {
                miesiac = "Listopad";
            }
            if (data.Text == "12")
            {
                miesiac = "Grudzień";
            }
        }
    }
}
