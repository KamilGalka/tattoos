﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Tattoos
{
    public partial class Form1 : Form
    {
       


         Igly _3RL_igla = new Igly();
         Igly _5RL_igla = new Igly();
         Igly _7RL_igla = new Igly();
         Igly _9RL_igla = new Igly();
         Igly _11RL_igla = new Igly();
         Igly _7RM_igla = new Igly();
         Igly _9RM_igla = new Igly();
         Igly _13RM_igla = new Igly();
         Igly _15RM_igla = new Igly();
         Igly _7MG_igla = new Igly();
         Igly _9MG_igla = new Igly();
         Igly _13MG_igla = new Igly();
         Igly _11RS_igla = new Igly();

        Rury _3RL_rura = new Rury();
        Rury _5RL_rura = new Rury();
        Rury _7RL_rura = new Rury();
        Rury _9RL_rura = new Rury();
        Rury _11RL_rura = new Rury();
        Rury _5FL_rura = new Rury();
        Rury _7FL_rura = new Rury();
        Rury _13FL_rura = new Rury();
        Rury _11FL_rura = new Rury();
        Rury _9FL_rura = new Rury();
        Rury _11DT_rura = new Rury();
       


        public Form1()
        {
            InitializeComponent();
           
            
            button1.MouseEnter += zmienKolorEnterb1;
            button1.MouseLeave += zmienKolorLeaveb1;
            button2.MouseEnter += zmienKolorEnterb2;
            button2.MouseLeave += zmienKolorLeaveb2;

            _3RL_igla.wypiszIlosc(label_3RL_igla_ilosc);
            _5RL_igla.wypiszIlosc(label_5RL_igla_ilosc);
            _7RL_igla.wypiszIlosc(label_7RL_igla_ilosc);
            _9RL_igla.wypiszIlosc(label_9RL_igla_ilosc);
            _11RL_igla.wypiszIlosc(label_11RL_igla_ilosc);
            _7RM_igla.wypiszIlosc(label_7RM_igla_ilosc);
            _9RM_igla.wypiszIlosc(label_9RM_igla_ilosc);
            _13RM_igla.wypiszIlosc(label_13RM_igla_ilosc);
            _15RM_igla.wypiszIlosc(label_15RM_igla_ilosc);
            _7MG_igla.wypiszIlosc(label_7MG_igla_ilosc);
            _9MG_igla.wypiszIlosc(label_9MG_igla_ilosc);
            _13MG_igla.wypiszIlosc(label_13MG_igla_ilosc);
            _11RS_igla.wypiszIlosc(label_11RS_igla_ilosc);

            _3RL_rura.wypiszIlosc(label_3RL_rura_ilosc);
            _5RL_rura.wypiszIlosc(label_5RL_rura_ilosc);
            _7RL_rura.wypiszIlosc(label_7RL_rura_ilosc);
            _9RL_rura.wypiszIlosc(label_9RL_rura_ilosc);
            _11RL_rura.wypiszIlosc(label_11RL_rura_ilosc);
            _5FL_rura.wypiszIlosc(label_5FL_rura_ilosc);
            _7FL_rura.wypiszIlosc(label_7FL_rura_ilosc);
            _13FL_rura.wypiszIlosc(label_13FL_rura_ilosc);
            _11FL_rura.wypiszIlosc(label_11FL_rura_ilosc);
            _9FL_rura.wypiszIlosc(label_9FL_rura_ilosc);
            _11DT_rura.wypiszIlosc(label_11DT_rura_ilosc);
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        
       
        private void zmienKolorEnterb1(object sender, EventArgs e)
        {
            button1.BackColor = SystemColors.GradientInactiveCaption;
            
        }
        private void zmienKolorLeaveb1(object sender, EventArgs e)
        {
            button1.BackColor = SystemColors.InactiveCaption;
        }
        private void zmienKolorEnterb2(object sender, EventArgs e)
        {
            button2.BackColor = SystemColors.GradientInactiveCaption;

        }
        private void zmienKolorLeaveb2(object sender, EventArgs e)
        {
            button2.BackColor = SystemColors.InactiveCaption;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int ilosc_zuzytych_igiel = 0;
            int ilosc_zuzytych_rur = 0;

          
            ilosc_zuzytych_igiel = int.Parse(numeric_3RL_igla_zuzyto.Text); 
            label_3RL_igla_ilosc.Text = _3RL_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_5RL_igla_zuzyto.Text);
            label_5RL_igla_ilosc.Text = _5RL_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_7RL_igla_zuzyto.Text);
            label_7RL_igla_ilosc.Text = _7RL_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_9RL_igla_zuzyto.Text);
            label_9RL_igla_ilosc.Text = _9RL_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_11RL_igla_zuzyto.Text);
            label_11RL_igla_ilosc.Text = _11RL_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_7RM_igla_zuzyto.Text);
            label_7RM_igla_ilosc.Text = _7RM_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_9RM_igla_zuzyto.Text);
            label_9RM_igla_ilosc.Text = _9RM_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_13RM_igla_zuzyto.Text);
            label_13RM_igla_ilosc.Text = _13RM_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_15RM_igla_zuzyto.Text);
            label_15RM_igla_ilosc.Text = _15RM_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_7MG_igla_zuzyto.Text);
            label_7MG_igla_ilosc.Text = _7MG_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_9MG_igla_zuzyto.Text);
            label_9MG_igla_ilosc.Text = _9MG_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_13MG_igla_zuzyto.Text);
            label_13MG_igla_ilosc.Text = _13MG_igla.odejmij(ilosc_zuzytych_igiel).ToString();

            ilosc_zuzytych_igiel = int.Parse(numeric_11RS_igla_zuzyto.Text);
            label_11RS_igla_ilosc.Text = _11RS_igla .odejmij(ilosc_zuzytych_igiel).ToString();

            MessageBox.Show("Zapisano");
            checkBox_3RL_igla.Checked = false;
            checkBox_5RL_igla.Checked = false;
            checkBox_7RL_igla.Checked = false;
            checkBox_9RL_igla.Checked = false;
            checkBox_11RL_igla.Checked = false;
            checkBox_7RM_igla.Checked = false;
            checkBox_9RM_igla.Checked = false;
            checkBox_13RM_igla.Checked = false;
            checkBox_15RM_igla.Checked = false;
            checkBox_7MG_igla.Checked = false;
            checkBox_9MG_igla.Checked = false;
            checkBox_13MG_igla.Checked = false;
            checkBox_11RS_igla.Checked = false;

            ilosc_zuzytych_rur = int.Parse(numeric_3RL_rura_zuzyto.Text);
            label_3RL_rura_ilosc.Text = _3RL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_5RL_rura_zuzyto.Text);
            label_5RL_rura_ilosc.Text = _5RL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_7RL_rura_zuzyto.Text);
            label_7RL_rura_ilosc.Text = _7RL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_9RL_rura_zuzyto.Text);
            label_9RL_rura_ilosc.Text = _9RL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_11RL_rura_zuzyto.Text);
            label_11RL_rura_ilosc.Text = _11RL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_5FL_rura_zuzyto.Text);
            label_5FL_rura_ilosc.Text = _5FL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_7FL_rura_zuzyto.Text);
            label_7FL_rura_ilosc.Text = _7FL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_13FL_rura_zuzyto.Text);
            label_13FL_rura_ilosc.Text = _13FL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_11FL_rura_zuzyto.Text);
            label_11FL_rura_ilosc.Text = _11FL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_9FL_rura_zuzyto.Text);
            label_9FL_rura_ilosc.Text = _9FL_rura.odejmij(ilosc_zuzytych_rur).ToString();

            ilosc_zuzytych_rur = int.Parse(numeric_11DT_rura_zuzyto.Text);
            label_11DT_rura_ilosc.Text = _11DT_rura.odejmij(ilosc_zuzytych_rur).ToString();
        }

        private void checkBox_3RL_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_3RL_igla.Checked) { 
                numeric_3RL_igla_zuzyto.Enabled = true;
                numeric_3RL_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_3RL_igla_zuzyto.Enabled = false;
                numeric_3RL_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_5RL_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_5RL_igla.Checked)
            {
                numeric_5RL_igla_zuzyto.Enabled = true;
                numeric_5RL_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_5RL_igla_zuzyto.Enabled = false;
                numeric_5RL_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_7RL_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_7RL_igla.Checked)
            {
                numeric_7RL_igla_zuzyto.Enabled = true;
                numeric_7RL_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_7RL_igla_zuzyto.Enabled = false;
                numeric_7RL_igla_zuzyto.Value = 0;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Igly.helper = 1;
            Rury.helper = 1;
            this.Hide();
            Start st = new Start();
            st.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_3RL_rura.Checked)
            {
                numeric_3RL_rura_zuzyto.Enabled = true;
                numeric_3RL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_3RL_rura_zuzyto.Enabled = false;
                numeric_3RL_rura_zuzyto.Value = 0;
            }
          
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_5RL_rura.Checked)
            {
                numeric_5RL_rura_zuzyto.Enabled = true;
                numeric_5RL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_5RL_rura_zuzyto.Enabled = false;
                numeric_5RL_rura_zuzyto.Value = 0;
            }
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_7RL_rura.Checked)
            {
                numeric_7RL_rura_zuzyto.Enabled = true;
                numeric_7RL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_7RL_rura_zuzyto.Enabled = false;
                numeric_7RL_rura_zuzyto.Value = 0;
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label_5RL_ilosc_Click(object sender, EventArgs e)
        {
            
        }

        private void checkBox_13MG_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_13MG_igla.Checked)
            {
                numeric_13MG_igla_zuzyto.Enabled = true;
                numeric_13MG_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_13MG_igla_zuzyto.Enabled = false;
                numeric_13MG_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_11RS_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_11RS_igla.Checked)
            {
                numeric_11RS_igla_zuzyto.Enabled = true;
                numeric_11RS_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_11RS_igla_zuzyto.Enabled = false;
                numeric_11RS_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_9RL_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_9RL_igla.Checked)
            {
                numeric_9RL_igla_zuzyto.Enabled = true;
                numeric_9RL_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_9RL_igla_zuzyto.Enabled = false;
                numeric_9RL_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_11RL_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_11RL_igla.Checked)
            {
                numeric_11RL_igla_zuzyto.Enabled = true;
                numeric_11RL_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_11RL_igla_zuzyto.Enabled = false;
                numeric_11RL_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_7RM_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_7RM_igla.Checked)
            {
                numeric_7RM_igla_zuzyto.Enabled = true;
                numeric_7RM_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_7RM_igla_zuzyto.Enabled = false;
                numeric_7RM_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_9RM_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_9RM_igla.Checked)
            {
                numeric_9RM_igla_zuzyto.Enabled = true;
                numeric_9RM_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_9RM_igla_zuzyto.Enabled = false;
                numeric_9RM_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_13RM_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_13RM_igla.Checked)
            {
                numeric_13RM_igla_zuzyto.Enabled = true;
                numeric_13RM_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_13RM_igla_zuzyto.Enabled = false;
                numeric_13RM_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_15RM_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_15RM_igla.Checked)
            {
                numeric_15RM_igla_zuzyto.Enabled = true;
                numeric_15RM_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_15RM_igla_zuzyto.Enabled = false;
                numeric_15RM_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_7MG_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_7MG_igla.Checked)
            {
                numeric_7MG_igla_zuzyto.Enabled = true;
                numeric_7MG_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_7MG_igla_zuzyto.Enabled = false;
                numeric_7MG_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox_9MG_igla_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_9MG_igla.Checked)
            {
                numeric_9MG_igla_zuzyto.Enabled = true;
                numeric_9MG_igla_zuzyto.Value = 1;
            }
            else
            {
                numeric_9MG_igla_zuzyto.Enabled = false;
                numeric_9MG_igla_zuzyto.Value = 0;
            }
        }

        private void checkBox20_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox20.Checked)
            {
                numeric_9RL_rura_zuzyto.Enabled = true;
                numeric_9RL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_9RL_rura_zuzyto.Enabled = false;
                numeric_9RL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_11RL_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_11RL_rura.Checked)
            {
                numeric_11RL_rura_zuzyto.Enabled = true;
                numeric_11RL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_11RL_rura_zuzyto.Enabled = false;
                numeric_11RL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_5FL_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_5FL_rura.Checked)
            {
                numeric_5FL_rura_zuzyto.Enabled = true;
                numeric_5FL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_5FL_rura_zuzyto.Enabled = false;
                numeric_5FL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_7FL_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_7FL_rura.Checked)
            {
                numeric_7FL_rura_zuzyto.Enabled = true;
                numeric_7FL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_7FL_rura_zuzyto.Enabled = false;
                numeric_7FL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_13FL_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_13FL_rura.Checked)
            {
                numeric_13FL_rura_zuzyto.Enabled = true;
                numeric_13FL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_13FL_rura_zuzyto.Enabled = false;
                numeric_13FL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_11FL_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_11FL_rura.Checked)
            {
                numeric_11FL_rura_zuzyto.Enabled = true;
                numeric_11FL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_11FL_rura_zuzyto.Enabled = false;
                numeric_11FL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_9FL_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_9FL_rura.Checked)
            {
                numeric_9FL_rura_zuzyto.Enabled = true;
                numeric_9FL_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_9FL_rura_zuzyto.Enabled = false;
                numeric_9FL_rura_zuzyto.Value = 0;
            }
        }

        private void checkBox_11DT_rura_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_11DT_rura.Checked)
            {
                numeric_11DT_rura_zuzyto.Enabled = true;
                numeric_11DT_rura_zuzyto.Value = 1;
            }
            else
            {
                numeric_11DT_rura_zuzyto.Enabled = false;
                numeric_11DT_rura_zuzyto.Value = 0;
            }
        }
    }
}
