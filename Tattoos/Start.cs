﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tattoos
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
            button1.MouseEnter += zmienKolorEnterb1;
            button1.MouseLeave += zmienKolorLeaveb1;
            button2.MouseEnter += zmienKolorEnterb2;
            button2.MouseLeave += zmienKolorLeaveb2;
        }
       

        
    private void Form1_Load(object sender, EventArgs e)
    {

    }

    private void zmienKolorEnterb1(object sender, EventArgs e)
    {
        button1.BackColor = SystemColors.GradientInactiveCaption;

    }
        private void zmienKolorLeaveb1(object sender, EventArgs e)
    {
        button1.BackColor = SystemColors.InactiveCaption;

    }
    private void zmienKolorEnterb2(object sender, EventArgs e)
    {
        button2.BackColor = SystemColors.GradientInactiveCaption;
    }
    private void zmienKolorLeaveb2(object sender, EventArgs e)
    {
            button2.BackColor = SystemColors.InactiveCaption;
    }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1();
            f1.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_tatuaz_Click(object sender, EventArgs e)
        {
            Tatuaz t1 = new Tatuaz();
            t1.Show();
            this.Hide();
        }

        private void button_dane_Click(object sender, EventArgs e)
        {
            this.Hide();
            Dane dn = new Dane();
            dn.Show();
        }

        private void button_zakupy_Click(object sender, EventArgs e)
        {
            this.Hide();
            Zakupy zk = new Zakupy();
            zk.Show();
        }
    }
}
