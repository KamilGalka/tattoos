﻿namespace Tattoos
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_tatuaz = new System.Windows.Forms.Button();
            this.button_dane = new System.Windows.Forms.Button();
            this.button_zakupy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button2.Location = new System.Drawing.Point(8, 224);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(276, 47);
            this.button2.TabIndex = 19;
            this.button2.Text = "Zamknij";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.Location = new System.Drawing.Point(12, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(276, 47);
            this.button1.TabIndex = 19;
            this.button1.Text = "Asortyment";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_tatuaz
            // 
            this.button_tatuaz.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_tatuaz.Location = new System.Drawing.Point(12, 12);
            this.button_tatuaz.Name = "button_tatuaz";
            this.button_tatuaz.Size = new System.Drawing.Size(276, 47);
            this.button_tatuaz.TabIndex = 20;
            this.button_tatuaz.Text = "Tatuaż";
            this.button_tatuaz.UseVisualStyleBackColor = false;
            this.button_tatuaz.Click += new System.EventHandler(this.button_tatuaz_Click);
            // 
            // button_dane
            // 
            this.button_dane.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_dane.Location = new System.Drawing.Point(12, 171);
            this.button_dane.Name = "button_dane";
            this.button_dane.Size = new System.Drawing.Size(276, 47);
            this.button_dane.TabIndex = 21;
            this.button_dane.Text = "Dane";
            this.button_dane.UseVisualStyleBackColor = false;
            this.button_dane.Click += new System.EventHandler(this.button_dane_Click);
            // 
            // button_zakupy
            // 
            this.button_zakupy.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_zakupy.Location = new System.Drawing.Point(8, 65);
            this.button_zakupy.Name = "button_zakupy";
            this.button_zakupy.Size = new System.Drawing.Size(276, 47);
            this.button_zakupy.TabIndex = 22;
            this.button_zakupy.Text = "Zakupy";
            this.button_zakupy.UseVisualStyleBackColor = false;
            this.button_zakupy.Click += new System.EventHandler(this.button_zakupy_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(296, 287);
            this.Controls.Add(this.button_zakupy);
            this.Controls.Add(this.button_dane);
            this.Controls.Add(this.button_tatuaz);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Name = "Start";
            this.Text = "Start";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_tatuaz;
        private System.Windows.Forms.Button button_dane;
        private System.Windows.Forms.Button button_zakupy;
    }
}