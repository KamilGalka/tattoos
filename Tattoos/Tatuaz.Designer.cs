﻿namespace Tattoos
{
    partial class Tatuaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_zamknij = new System.Windows.Forms.Button();
            this.button_zapisz = new System.Windows.Forms.Button();
            this.comboBox_rok = new System.Windows.Forms.ComboBox();
            this.comboBox_miesiac = new System.Windows.Forms.ComboBox();
            this.comboBox_dzien = new System.Windows.Forms.ComboBox();
            this.textBox_opis = new System.Windows.Forms.TextBox();
            this.textBox_kwota = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_zamknij
            // 
            this.button_zamknij.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_zamknij.Location = new System.Drawing.Point(9, 240);
            this.button_zamknij.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(309, 38);
            this.button_zamknij.TabIndex = 20;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = false;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // button_zapisz
            // 
            this.button_zapisz.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_zapisz.Location = new System.Drawing.Point(9, 197);
            this.button_zapisz.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_zapisz.Name = "button_zapisz";
            this.button_zapisz.Size = new System.Drawing.Size(309, 38);
            this.button_zapisz.TabIndex = 21;
            this.button_zapisz.Text = "Zapisz";
            this.button_zapisz.UseVisualStyleBackColor = false;
            this.button_zapisz.Click += new System.EventHandler(this.button_zapisz_Click);
            // 
            // comboBox_rok
            // 
            this.comboBox_rok.FormattingEnabled = true;
            this.comboBox_rok.Items.AddRange(new object[] {
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.comboBox_rok.Location = new System.Drawing.Point(87, 5);
            this.comboBox_rok.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox_rok.Name = "comboBox_rok";
            this.comboBox_rok.Size = new System.Drawing.Size(50, 21);
            this.comboBox_rok.TabIndex = 22;
            this.comboBox_rok.Text = "2018";
            // 
            // comboBox_miesiac
            // 
            this.comboBox_miesiac.FormattingEnabled = true;
            this.comboBox_miesiac.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboBox_miesiac.Location = new System.Drawing.Point(142, 5);
            this.comboBox_miesiac.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox_miesiac.Name = "comboBox_miesiac";
            this.comboBox_miesiac.Size = new System.Drawing.Size(38, 21);
            this.comboBox_miesiac.TabIndex = 23;
            this.comboBox_miesiac.Text = "01";
            // 
            // comboBox_dzien
            // 
            this.comboBox_dzien.FormattingEnabled = true;
            this.comboBox_dzien.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.comboBox_dzien.Location = new System.Drawing.Point(183, 5);
            this.comboBox_dzien.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox_dzien.Name = "comboBox_dzien";
            this.comboBox_dzien.Size = new System.Drawing.Size(36, 21);
            this.comboBox_dzien.TabIndex = 24;
            this.comboBox_dzien.Text = "01";
            // 
            // textBox_opis
            // 
            this.textBox_opis.Location = new System.Drawing.Point(87, 41);
            this.textBox_opis.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_opis.MaxLength = 100;
            this.textBox_opis.Name = "textBox_opis";
            this.textBox_opis.Size = new System.Drawing.Size(234, 20);
            this.textBox_opis.TabIndex = 25;
            this.textBox_opis.TextChanged += new System.EventHandler(this.textBox_opis_TextChanged);
            // 
            // textBox_kwota
            // 
            this.textBox_kwota.Location = new System.Drawing.Point(87, 80);
            this.textBox_kwota.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_kwota.MaxLength = 100;
            this.textBox_kwota.Name = "textBox_kwota";
            this.textBox_kwota.Size = new System.Drawing.Size(72, 20);
            this.textBox_kwota.TabIndex = 26;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(9, 7);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(30, 13);
            this.Label1.TabIndex = 27;
            this.Label1.Text = "Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 44);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Opis";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 82);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Kwota";
            // 
            // Tatuaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(327, 284);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.textBox_kwota);
            this.Controls.Add(this.textBox_opis);
            this.Controls.Add(this.comboBox_dzien);
            this.Controls.Add(this.comboBox_miesiac);
            this.Controls.Add(this.comboBox_rok);
            this.Controls.Add(this.button_zapisz);
            this.Controls.Add(this.button_zamknij);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Tatuaz";
            this.Text = "Tatuaz";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Button button_zapisz;
        private System.Windows.Forms.ComboBox comboBox_rok;
        private System.Windows.Forms.ComboBox comboBox_miesiac;
        private System.Windows.Forms.ComboBox comboBox_dzien;
        private System.Windows.Forms.TextBox textBox_opis;
        private System.Windows.Forms.TextBox textBox_kwota;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}